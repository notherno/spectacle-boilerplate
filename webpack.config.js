const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const srcDirName = path.join(__dirname, 'src')
const publicDirName = path.join(__dirname, 'public')

module.exports = {
  devtool: 'inline-source-map',
  entry: ['babel-polyfill', path.resolve(srcDirName, 'index.jsx')],
  output: {
    path: publicDirName,
    filename: 'bundle.js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(srcDirName, 'index.html'),
    }),
  ],
  performance: {
    hints: false,
  },
  devServer: {
    contentBase: publicDirName,
    port: 3000,
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.md$/,
        use: [
          { loader: 'html-loader' },
          {
            loader: 'markdown-loader',
            options: {
              gfm: false,
            },
          },
        ],
      },
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [{ loader: 'style-loader' }, { loader: 'css-loader' }],
      },
      {
        test: /\.html$/,
        use: [{ loader: 'html-loader' }],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              mimetype: 'image/svg+xml',
            },
          },
        ],
      },
      {
        test: /\.png$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              mimetype: 'image/png',
            },
          },
        ],
      },
      {
        test: /\.gif$/,
        use: [{ loader: 'url-loader', options: { mimetype: 'image/gif' } }],
      },
      {
        test: /\.jpg$/,
        use: [{ loader: 'url-loader', options: { mimetype: 'image/jpg' } }],
      },
    ],
  },
}
